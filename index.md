---
layout: mine
title: The Attic
---

Most of the site and programs from the site are licensed under the [WTFPLv2 license](wtfpl).

# Tools #

- [FLDev](fldev): expose partitions of a disk image in a virtual filesystem
- [SIT-Tagger](sit-tagger): a simple image browser/viewer with tagging


# Command-line tools and one-liners #

- [hexgen](hexgen): generate data from an hex dump
- [tailsleep](tailsleep): like tail -f but quits when I/O activity stops
- [univisible](univisible): tweak Unicode combinations and visualize them
- [wakeonwan](wakeonwan): wake remote machines with Wake-on-WAN
- [iqontool](iqontool): pack multiple images into a .ico or .icns
- [random-line](random-line): take a random line from stdin
- [qunpak](qunpak): extract Quake I and II .pak files
- [keepassxprint](keepassxprint): dump info and passwords from a KeePassX database
- [redmine2ical](redmine2ical): convert Redmine's timesheet to iCalendar format
- [timecalc](timecalc): calculator of dates and durations
- [morse](morse): text from/to Morse code converter, and optional beep player


## JSON helpers ##

- [json2sqlite](jsontools/json2sqlite.html): insert JSON data in SQLite
- [nested-jq](jsontools/nested-jq.html): make [jq](https://stedolan.github.io/jq/) parsed nested JSON content
- [json2table](jsontools/json2table.html): pretty-print JSON data (list of objects) in an ASCII-art table
- [json2csv](jsontools/json2csv.html): convert JSON data (a list of objects) to CSV


# Tiny graphical helpers #

- [qr-shot](qr-shot): decode a QR code image from part of the screen
- [coordapp](coordapp): always-on-top window that shows the mouse cursor coordinates
- [qruler](qruler): tool window that measures width and height in pixels


# Fun with graphics #

- [ImageMagick stuff](magick): (including "how to read a CAPTCHA on console with ImageMagick")
- [image2xterm](image2xterm): display an image on console using terminal RGB24 mode or 256 colors
- [fonts2png](fonts2png): render TTF fonts samples to image files
- [qr2unicode](qr2unicode): display QR-codes on console using Unicode box-drawing characters
- [Some images](gfx)


# Libraries #

- [vignette](https://github.com/hydrargyrum/vignette): a Python library for generating thumbnails following the FreeDesktop specification [[documentation](https://vignette.readthedocs.io)]
- [qorbeille](https://github.com/hydrargyrum/qorbeille): a Qt library to trash files to recycle bin
- [qvariantjson](https://github.com/hydrargyrum/qvariantjson): yet another Qt4 JSON library
- [C++ ASCII tree](cppasciitree): an example of how to hardcode a tree with source code looking like the actual tree
- a patch for upgrading [pycairo to cairo 0.12](py2cairo)


# Various plugins #

- [autocopier](r2w_plugins): rest2web plugin: automatically marks referenced images and files for inclusion at deployment
- [rss](r2w_plugins): rest2web plugin: generate RSS feeds along with your pages
- [supybot-shell](supybot-shell): Supybot plugin: execute shell commands and see their output


# Articles #

- [Unicode in filenames in OSes](misc/unicode-filenames.html)
- [Extract music from some video games](misc/extract-vg-music.html)
- [Display Velib stations positions in OsmAnd](misc/velib-gpx-osmand.html)
- [Variables and quoting in shell](misc/shell-quoting.html)
- [git tips](misc/git-tips.html)
