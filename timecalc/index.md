---
layout: mine
title: timecalc
---

# timecalc

A calculator of dates and durations.

`timecalc` can make various calculations like adding durations to dates and times, computing the duration difference between two dates and times, etc. It takes textual expressions.

```
% timecalc "2015/01/01 + 1 day, 2 hours"
2015/01/02 02:00
```

Expressions can be given either on the command-line or in a Read-Eval-Print-Loop, if no argument is given.

# Download #

[Project repository](https://github.com/hydrargyrum/timecalc)

timecalc uses Python 2 and is licensed under the [WTFPLv2](../wtfpl).
